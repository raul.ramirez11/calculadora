
import java.util.Scanner;
/**
 * <h2>clase Validator, se utiliza para Validar los valores introducidos en la clase Program </h2>
 * 
 * @version 1-2020
 * @author Raul Ramirez
 * @since 28/01/2020
 */



public class Validator {
/**
 * ValidateOption revisa si n es correcta y si es correcta te devuelve rvalidated
 * @param n numero que quieres validar
 * @return rvalidated  
 */
	public int ValidateOption (int n)
	{
		int rvalidated = 0;
		int state = 0;
		do
		{			
			Scanner reader = new Scanner (System.in);
			n= reader.nextInt();
			reader.close();
			if (n < 1 || n > 4) System.out.println("Error: El valor debe estar entre 1 y el 4");
			else state = 1;
		}while (state != 1);
		rvalidated = n;

		return rvalidated;
	}
}
